#! python3

# Simple program that convert file input to a vscode snippet.
# It will generate a file Snippet.txt, copy past content
# made by Jonathan.C 12/06/2020


# 1 - Take a file as input and ask info
filename = input("Filename: ")
title = input("Snippet title: ")
description = input("Short description: ")

extention = filename.split('.')[1]

# 2 - open file body source
snippet_source = open(filename, "r+")
fLines = snippet_source.readlines()

# 3 - create Snippet.txt to save the formated snippet
snippet_formatted = open("Snippet.txt", "w")

# 4 - add meta on top
start = ("""
"Print to console": {
    "scope": "%s",
    "prefix": "%s",
    "description": "%s",
    "body": [
""" % (extention, title, description))

snippet_formatted.write(start)

# 5 - Add "<code>", on each lines
for line in fLines:
    line = line.replace('\n', '')
    snippet_formatted.write("      " + "\"" + line + "\",\n")

snippet_formatted.write("      \"\",\n")

# 6 - Ended nicely the file
snippet_formatted.write(
"""
    ],
}

"""
)

snippet_formatted.close()
snippet_source.close()
